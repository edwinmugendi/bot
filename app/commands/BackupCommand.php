<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class BackupCommand extends Command {

    //Name
    protected $name = 'command:backup';
    //Decription
    protected $description = 'Backup media and db';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire() {
        $mediaController = new Lava\Media\MediaController();

        if ($this->argument('db_or_media') == 'db') {
            $mediaController->backupDb();
        } else if ($this->argument('db_or_media') == 'media') {
            $mediaController->backupMedia();
        }//E# if else statement
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments() {
        return array(
            array('db_or_media', InputArgument::REQUIRED, 'Db or media.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions() {
        return array(
            array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }

}
