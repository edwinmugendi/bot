<?php

namespace Lava\Surveys;

/**
 * S# OptionController() function
 * Option controller
 * @author Edwin Mugendi
 */
class OptionController extends SurveysBaseController {

    //Controller
    public $controller = 'option';

}

//E# OptionController() function