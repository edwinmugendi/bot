<?php

namespace Lava\Surveys;

use Carbon\Carbon;

/**
 * S# BotController() function
 * Bot controller
 * @author Edwin Mugendi
 */
class BotController extends SurveysBaseController {

    //Controller
    public $controller = 'bot';
    //Channel
    private $channel;
    //Message
    public $message_id;
    public $recipient_id;
    //Text
    public $text;

    /**
     * S# webhookBot() function
     * 
     * Bot webhook
     * 
     */
    public function webhookBot() {

        $segments = \Request::segments();

        \Log::info(json_encode($this->input));

        if (is_array($segments)) {
            if ($segments[0] == 'tg') {
                $this->channel = 'telegram';
                $this->message_id = $this->input['update_id'];
                if (array_key_exists('callback_query', $this->input)) {
                    $this->recipient_id = $this->input['callback_query']['from']['id'];
                    $message_json = json_encode($this->input['callback_query']);
                    //Set text to update    
                    $this->text = $this->input['callback_query']['data'];
                } else {
                    $this->recipient_id = $this->input['message']['chat']['id'];
                    $message_json = json_encode($this->input['message']);
                    //Set text to update    
                    $this->text = array_key_exists('text', $this->input['message']) ? $this->input['message']['text'] : '';
                }//E# if else statement
            } else if ($segments[0] == 'mg') {
                $this->channel = 'messenger';
                $this->message_id = $this->input['entry'][0]['messaging'][0]['message']['mid'];
                $this->recipient_id = $this->input['entry'][0]['messaging'][0]['sender']['id'];
                $message_json = json_encode($this->input['entry']);

                $this->text = '';
                if (array_key_exists('quick_reply', $this->input['entry'][0]['messaging'][0]['message'])) {
                    echo 'Second';
                    $this->text = $this->input['entry'][0]['messaging'][0]['message']['quick_reply']['payload'];
                } else if (array_key_exists('text', $this->input['entry'][0]['messaging'][0]['message'])) {
                    echo 'First';
                    $this->text = $this->input['entry'][0]['messaging'][0]['message']['text'];
                }//E# if else statement
            }//E# if else statement
        }//E# if statement

        \Log::info($this->channel . ' ' . json_encode($this->input));
        //Update array
        $update_array = array(
            'channel' => $this->channel,
            'type' => 'incoming',
            'update_id' => $this->message_id,
            'recipient_id' => $this->recipient_id,
            'message' => $message_json,
            /* 'agent' => $this->input['agent'], */
            'status' => 1,
            'created_by' => 1,
            'updated_by' => 1
        );

        //Fields to select
        $fields = array('*');

        //Set where clause
        $where_clause = array(
            array(
                'where' => 'where',
                'column' => 'update_id',
                'operator' => '=',
                'operand' => $this->message_id
            ),
            array(
                'where' => 'where',
                'column' => 'channel',
                'operator' => '=',
                'operand' => $this->channel
            )
        );

        //Set scope
        $parameters['scope'] = array('statusOne');

        //Select update
        $update_model = $this->callController(\Util::buildNamespace('surveys', 'update', 1), 'select', array($fields, $where_clause, 1, $parameters));

        if (!$update_model) {
            //Create update
            $update_model = $this->callController(\Util::buildNamespace('surveys', 'update', 1), 'createIfValid', array($update_array, true));
        }//E# if statement
        //HERE:
        if ((array_key_exists('entry', $this->input) && (array_key_exists('text', $this->input['entry'][0]['messaging'][0]['message']) && ((substr($this->input['entry'][0]['messaging'][0]['message']['text'], 0, 5) == '/fill') || (substr($this->input['entry'][0]['messaging'][0]['message']['text'], 0, 6) == '/start') ))) || (array_key_exists('message', $this->input) && array_key_exists('entities', $this->input['message']) && $this->input['message']['entities'][0]['type'] == 'bot_command')) {
            echo 'Command';
            \Log::info('Command');
            $this->processCommands();

            //  return json_encode(array('here'));
        } else {
            echo 'Answer';
            \Log::info('Answer');
            $this->processAnswer();
        }//E# if statement

        header('Content-Type: application/json');

        return array('chat_id' => $this->message_id);
    }

//E# webhookBot() function

    /**
     * S# processAnswer() function
     * 
     * Process answer
     * 
     */
    private function processAnswer() {
        $parameters = array();

        //Set scope
        $parameters['scope'] = array('statusOne');

        //Order by
        $parameters['orderBy'][] = array('updated_at' => 'asc');

        //Select session
        $session_model = $this->callController(\Util::buildNamespace('surveys', 'session', 1), 'getModelByField', array('channel_chat_id', $this->recipient_id, $parameters));

        if ($session_model) {

            $session_model->updated_at = Carbon::now();

            $session_model->save();

            if ($session_model->next_question == ($session_model->total_questions)) {

                //Data to update
                $data_to_update = array(
                    'workflow' => 'y',
                );

                //Update actual form
                $actual_form_model = $this->callController(\Util::buildNamespace('forms', \Str::lower(str_replace('_', ' ', $session_model->form->name)), 1), 'updateIfValid', array('id', $session_model->actual_form_id, $data_to_update, true));

                //Emoticons
                $emoticons = "\uD83D\uDC4D";

                $parameters = array(
                    'type' => 'text',
                    'chat_id' => $this->recipient_id,
                    'text' => 'Thanks for completing this survey ' . json_decode('"' . $emoticons . '"'),
                );

                $this->callController(\Util::buildNamespace('surveys', $this->channel, 1), 'sendMessage', array($parameters));

                return '';
            } else {
                $parameters = array();

                //Set scope
                $parameters['scope'] = array('statusOne');

                //Select form
                $form_model = $this->callController(\Util::buildNamespace('surveys', 'form', 1), 'getModelByField', array('id', $session_model->form_id, $parameters));

                $question_model = $form_model->questions[$session_model->next_question];

                echo $question_model->name;
                $is_valid = $this->validateResponse($question_model);
                //dd($is_valid);
                if ($is_valid) {

                    $data_to_update = $this->setDataToUpdate($session_model, $form_model, $question_model);

                    //dd($data_to_update);;
                    if ($session_model->next_question < ($session_model->total_questions)) {
                        $session_model->next_question += 1;

                        if ($session_model->next_question == $session_model->total_questions) {
                            $data_to_update['workflow'] = 'complete';
                        }//E# if statement

                        $session_model->save();
                    }//E# if statement
                    //dd($data_to_update);
                    // $data_to_update = array('gender' => 1);
                    //  var_dump($data_to_update);
                    //  var_dump($session_model->actual_form_id);
                    //Update actual form
                    $actual_form_model = $this->callController(\Util::buildNamespace('forms', $form_model->name, 1), 'updateIfValid', array('id', $session_model->actual_form_id, $data_to_update, true));

                    $this->sendNextQuestion($form_model, $session_model);
                } else {
                    $parameters = array(
                        'type' => 'text',
                        'chat_id' => $this->recipient_id,
                        'text' => $question_model->error_message
                    );

                    $this->callController(\Util::buildNamespace('surveys', $this->channel, 1), 'sendMessage', array($parameters));
                }//E# if else statement
            }//E# if else statement
        } else {
            //Emoticons
            $emoticons = "\xf0\x9f\x98\xaf";

            $parameters = array(
                'type' => 'text',
                'chat_id' => $this->recipient_id,
                'text' => json_decode('"' . $emoticons . '"') . ' Oops! form not found. Please design form on surveychat.co and type /fill {form name} eg /fill contact'
            );

            $this->callController(\Util::buildNamespace('surveys', $this->channel, 1), 'sendMessage', array($parameters));
        }//E# if else statement
    }

//E# processAnswer() function

    /**
     * S# setDataToUpdate() function
     * 
     * Set data to update
     * 
     * @param Model $session_model Session Model
     * @param Model $form_model Form Model
     * @param Model $question_model Question Model
     * 
     * @return array Data to update
     */
    private function setDataToUpdate($session_model, $form_model, $question_model) {
        $data_to_update = array();
        // var_dump($question_model['type']);
        switch ($question_model['type']) {
            case 'text': {
                    $data_to_update['' . $question_model->name . ''] = $this->text;
                    break;
                }//E# case
            case 'integer': {
                    $data_to_update['' . $question_model->name . ''] = $this->text;
                    break;
                }//E# case
            case 'decimal': {
                    $data_to_update['' . $question_model->name . ''] = $this->text;
                    break;
                }//E# case
            case 'photo': {
                    if ($this->channel == 'telegram') {
                        $photos = $this->input['message']['photo'];
                    } else if ($this->channel == 'messenger') {
                        $photos = $this->input['entry'][0]['messaging'][0]['message']['attachments'];
                    }//E# if else statement
                    // var_dump($photos);
                    $photos_count = count($photos);

                    $this->callController(\Util::buildNamespace('surveys', $this->channel, 1), 'savePhoto', array($session_model, $form_model, $question_model, $photos[($photos_count - 1)]));

                    break;
                }//E# case
            case 'gps': {
                    if ($this->channel == 'telegram') {
                        $data_to_update['lat'] = $this->input['message']['location']['latitude'];
                        $data_to_update['lng'] = $this->input['message']['location']['longitude'];
                    } else if ($this->channel == 'messenger') {
                        $data_to_update['lat'] = $this->input['entry'][0]['messaging'][0]['message']['attachments'][0]['payload']['coordinates']['lat'];
                        $data_to_update['lng'] = $this->input['entry'][0]['messaging'][0]['message']['attachments'][0]['payload']['coordinates']['long'];
                    }//E# if else statement
                    break;
                }//E# case
            case 'radio': {
                    $data_to_update['' . $question_model->name . ''] = $this->text;
                    break;
                }//E# case
            case 'checkbox': {
                    return true;
                    break;
                }//E# case

            default:
                return true;
                break;
        }//E# switch statement

        return $data_to_update;
    }

//E# setDataToUpdate() function

    /**
     * S# validateResponse() function
     * 
     * Validate response
     * 
     * @param Model $question_model Question model
     * 
     */
    private function validateResponse($question_model) {

        switch ($question_model['type']) {
            case 'text': {
                    return true;
                    break;
                }//E# case
            case 'integer': {
                    return ctype_digit((string) $this->text);
                    break;
                }//E# case
            case 'decimal': {
                    return is_numeric($this->text);
                    break;
                }//E# case
            case 'photo': {
                    if ($this->channel == 'telegram') {
                        return (array_key_exists('photo', $this->input['message'])) && (count($this->input['message']['photo']) > 0);
                    } else if ($this->channel == 'messenger') {
                        return (array_key_exists('attachments', $this->input['entry'][0]['messaging'][0]['message'])) && (count($this->input) > 0);
                    }//E# if else statement
                    break;
                }//E# case
            case 'gps': {
                    if ($this->channel == 'telegram') {
                        return (array_key_exists('location', $this->input['message']) && array_key_exists('latitude', $this->input['message']['location']) && array_key_exists('longitude', $this->input['message']['location']));
                    } else if ($this->channel == 'messenger') {
                        return array_key_exists('lat', $this->input['entry'][0]['messaging'][0]['message']['attachments'][0]['payload']['coordinates']) && array_key_exists('long', $this->input['entry'][0]['messaging'][0]['message']['attachments'][0]['payload']['coordinates']);
                    }//E# if else statement
                    break;
                }//E# case
            case 'radio': {
                    $option_values = $question_model->options->lists('name');

                    return in_array($this->text, $option_values);
                    break;
                }//E# case
            case 'checkbox': {
                    return true;
                    break;
                }//E# case

            default:
                return true;
                break;
        }//E# switch statement
    }

//E# validateResponse() function

    /**
     * S# processCommandFill() function
     * 
     * Process Command Fill
     * 
     */
    private function processCommandFill() {

        $form = $this->callController(\Util::buildNamespace('surveys', $this->channel, 1), 'getFormText');

        if ($this->channel == 'telegram') {
            $names = $this->input['message']['from']['first_name'];

            if (array_key_exists('last_name', $this->input['message']['from'])) {
                $names .= $this->input['message']['from']['last_name'];
            }//E# if statement
        } else {
            $names = '';
        }//E# if else statement
        //Fields to select
        $fields = array('*');

        //Set where clause
        $where_clause = array(
            array(
                'where' => 'where',
                'column' => 'name',
                'operator' => '=',
                'operand' => $form
            ),
            array(
                'where' => 'where',
                'column' => 'workflow',
                'operator' => '=',
                'operand' => 'approved'
            ),
        );

        $parameters = array();
        //Set scope
        $parameters['lazyLoad'] = array('questions');

        //Set scope
        $parameters['scope'] = array('statusOne');

        //Select form
        $form_model = $this->callController(\Util::buildNamespace('surveys', 'form', 1), 'select', array($fields, $where_clause, 1, $parameters));

        if ($form_model) {

            //Fields to select
            $fields = array('*');

            //Set where clause
            $where_clause = array(
                array(
                    'where' => 'where',
                    'column' => 'form_id',
                    'operator' => '=',
                    'operand' => $form_model->id
                ),
                array(
                    'where' => 'where',
                    'column' => 'channel_chat_id',
                    'operator' => '=',
                    'operand' => $this->recipient_id
                )
            );

            $parameters = array();

            //Set scope
            $parameters['scope'] = array('statusOne');

            //Select session
            $session_model = $this->callController(\Util::buildNamespace('surveys', 'session', 1), 'select', array($fields, $where_clause, 1, $parameters));

            if (!$session_model) {

                $actual_form_array = array(
                    'organization_id' => $form_model->organization_id,
                    'form_id' => $form_model->id,
                    'channel' => $this->channel,
                    'channel_chat_id' => $this->recipient_id,
                    'names' => $names,
                    'status' => 1,
                    'created_by' => 1,
                    'updated_by' => 1,
                );

                //Create actual form 
                $actual_form_model = $this->callController(\Util::buildNamespace('forms', $form_model->name, 1), 'createIfValid', array($actual_form_array, true));

                $session_array = array(
                    'actual_form_id' => $actual_form_model->id,
                    'organization_id' => $form_model->organization_id,
                    'form_id' => $form_model->id,
                    'channel' => $this->channel,
                    'channel_chat_id' => $this->recipient_id,
                    'next_question' => 0,
                    'total_questions' => count($form_model['questions']),
                    'full_name' => $names,
                    'status' => 1,
                    'created_by' => 1,
                    'updated_by' => 1,
                );

                //Create session
                $session_model = $this->callController(\Util::buildNamespace('surveys', 'session', 1), 'createIfValid', array($session_array, true));

                //Update session id
                $actual_form_model->session_id = $session_model->id;
            } else {
                //Session
                $session_array = array(
                    'next_question' => 0,
                    'full_name' => $names,
                    'total_questions' => count($form_model['questions']),
                );

                //Update session
                $session_model = $this->callController(\Util::buildNamespace('surveys', 'session', 1), 'updateIfValid', array('id', $session_model->id, $session_array, true));
            }//E# if else statement

            if ($session_model->next_question == 0 && count($form_model->media)) {
                /*
                  $link = asset('media/lava/upload/' . $form_model->media[0]['name']);

                  $parameters = array(
                  'type' => 'photo',
                  'chat_id' => $this->input['message']['chat']['id'],
                  'link' => $link,
                  );

                  $this->sendMessage($parameters);
                 */
            }

            //Send next question
            $this->sendNextQuestion($form_model, $session_model);
        } else {
            //Emoticons
            $emoticons = "\xf0\x9f\x98\xaf";

            $parameters = array(
                'type' => 'text',
                'chat_id' => $this->recipient_id,
                'text' => json_decode('"' . $emoticons . '"') . ' Oops, form not found.Please design form on surveychat.co and type "/fill {form name}" try /fill contact'
            );

            return $this->callController(\Util::buildNamespace('surveys', $this->channel, 1), 'sendMessage', array($parameters));
        }//E# if else statement
    }

//E# processCommandFill() function

    /**
     * S# processCommandStart() function
     * 
     * Process Command Start
     * 
     */
    private function processCommandStart() {

        //Emoticons
        $emoticons = "\xf0\x9f\x91\x8b";

        $parameters = array(
            'type' => 'text',
            'chat_id' => $this->recipient_id,
            'text' => json_decode('"' . $emoticons . '"') . ' Welcome to ' . \Config::get('product.name') . '. To respond to a survey, type "/fill {form name}" try /fill contact'
        );
        echo json_encode($parameters);

        return $this->callController(\Util::buildNamespace('surveys', $this->channel, 1), 'sendMessage', array($parameters));
    }

//E# processCommandStart() function

    /**
     * S# sendNextQuestion() function
     * 
     * Send next question
     * 
     * @param Model $form_model Form Model
     * @param Model $session_model Session model
     * 
     */
    private function sendNextQuestion($form_model, $session_model) {
        if ($session_model->next_question == 0 && $form_model->welcome_message) {
            $parameters = array(
                'type' => 'text',
                'chat_id' => $this->recipient_id,
                'text' => $form_model->welcome_message,
            );

            $this->callController(\Util::buildNamespace('surveys', $this->channel, 1), 'sendMessage', array($parameters));
        }//E# if statement
        if ($session_model->next_question == $session_model->total_questions) {

            //Emoticons
            $emoticons = "\uD83D\uDC4D";

            $parameters = array(
                'type' => 'text',
                'chat_id' => $this->recipient_id,
                'text' => 'Thanks for completing this survey ' . json_decode('"' . $emoticons . '"'),
            );
        } else {
            $parameters = $this->callController(\Util::buildNamespace('surveys', $this->channel, 1), 'buildNextQuestion', array($form_model, $session_model, $this->recipient_id));
        }//E# if else statement

        $this->callController(\Util::buildNamespace('surveys', $this->channel, 1), 'sendMessage', array($parameters));
    }

//E# sendNextQuestion() function

    /**
     * S# processCommands() function
     * 
     * Process commands
     * 
     */
    private function processCommands() {
        //Get Command
        $command = $this->callController(\Util::buildNamespace('surveys', $this->channel, 1), 'getCommand');
        \Log::info($command);
        echo $command;
        switch ($command) {
            case '/start': {
                    $this->processCommandStart();
                    break;
                }//E# case statement
            case '/fill': {
                    $this->processCommandFill();
                    break;
                }//E# case statement
            default:
        }//E# switch statement

        return true;
    }

//E# processCommands() function

    /**
     * S# getFormText() function
     * 
     * Get form text
     * 
     * @return str form text
     */
    private function getFormText() {
        return trim(substr($this->input['message']['text'], ($this->input['message']['entities'][0]['length'] + 1), (strlen($this->input['message']['text']) - 1)));
    }

//E# getFormText() function
}

//E# BotController() function