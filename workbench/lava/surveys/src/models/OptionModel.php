<?php

namespace Lava\Surveys;

/**
 * S# OptionModel() Class
 * @author Edwin Mugendi
 * Option Model
 */
class OptionModel extends \BaseModel {

    //Table
    protected $table = 'svy_options';
    //View fields
    public $viewFields = array(
        'id' => array(1, 'text', '='),
        'name' => array(1, 'text', 'like', 1),
        'title' => array(1, 'text', 'like', 1),
        'value' => array(1, 'text', 'like', 1),
    );
    //Fillable fields
    protected $fillable = array(
        'id',
        'name',
        'organization_id',
        'form_id',
        'question_id',
        'user_id',
        'title',
        'value',
        'agent',
        'ip',
        'status',
        'created_by',
        'updated_by'
    );
    //Appends fields
    protected $appends = array(
    );
    //Hidden fields
    protected $hidden = array();
    //Create validation rules
    public $createRules = array(
        'name' => 'required',
        'title' => 'required',
        'value' => 'required',
    );
    //Update validation rules
    public $updateRules = array(
        'name' => 'required',
        'title' => 'required',
        'value' => 'required',
    );

}

//E# SurveyModel() Class