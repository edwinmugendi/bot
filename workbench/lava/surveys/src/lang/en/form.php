<?php

return array(
    /*
      |--------------------------------------------------------------------------
      | Form Language Lines
      |--------------------------------------------------------------------------
     */
    'data' => array(
        'channel' => array(
            '' => 'Select',
            'telegram' => 'Telegram',
            'messenger' => 'Facebook Messenger',
        ),
        'workflow' => array(
            '' => 'Select',
            'approved' => 'Approved',
            'published' => 'Pending approval',
            'unpublished' => 'Unpublished',
        ),
        'email_response' => array(
            '' => 'Select',
            'y' => 'Yes',
            'n' => 'No',
        ),
    ),
    'notification' => array(
        'created' => 'Form created',
        'updated' => 'Form updated',
        'deleted' => 'Form deleted',
    ),
    'view' => array(
        'menu' => 'Forms',
        'modal_heading' => 'Options',
        'field' => array(
            'id' => '#',
            'welcome_message' => 'Welcome message',
            'email_response' => 'Email response?',
            'email_response_to' => 'Email response to',
            'name' => 'Name',
            'workflow' => 'Status',
            'responses' => '# of responses',
        ),
        'actions' => array(
            'build' => array(
                'build' => 'Build form',
            ),
            'approve' => array(
                'confirm' => 'Approve form?',
                'approveMany' => 'Approved :count form',
                'confirmMany' => 'Approved :count form?',
                'approve' => 'Approve',
                'cancel' => 'Cancel',
                'by' => 'Approved by :name'
            ),
            'publish' => array(
                'confirm' => 'Seek approve?',
                'publishMany' => 'Published :count form',
                'confirmMany' => 'Publish :count form?',
                'publish' => 'Seek approve',
                'cancel' => 'Cancel',
                'by' => 'Survey Chat team will immediately review this survey and revert'
            ),
            'unpublish' => array(
                'confirm' => 'Un-publish form?',
                'unpublishMany' => 'Un-published :count form',
                'confirmMany' => 'Un-publish :count form?',
                'unpublish' => 'Un-publish',
                'cancel' => 'Cancel',
                'by' => 'Un-published by :name'
            ),
            'delete' => array(
                'confirm' => 'Delete form?',
                'deleteMany' => 'Deleted :count forms',
                'confirmMany' => 'Delete :count forms?',
                'delete' => 'Delete',
                'cancel' => 'Cancel',
            ),
            'undelete' => array(
                'undoDelete' => 'Undo delete',
                'undeleting' => 'Un deleting...',
                'undeleted' => 'Un deleted :count forms',
            ),
            'edit_questions' => array(
                'edit_questions' => 'Edit questions'
            ),
        ),
        'link' => array(
            'list' => 'Forms list',
            'add' => 'Add form',
            'found' => '{0} :count forms | {1} :count form | [2,Inf] :count forms',
        )
    ),
    'formQuestionPage' => array(
        'title' => 'Form: :title #:id',
    ),
    'formDetailedPage' => array(
        'title' => 'Form: :title #:id'
    ),
    'formListPage' => array(
        'title' => 'List of forms'
    ),
    'formPostPage' => array(
        'actionTitle' => array(
            1 => 'Create forms',
            2 => 'Update forms'
        ),
        'formPostView' => array(
            'form' => array(
                'media' => array(
                    'count' => 10,
                    'describe' => 0,
                    'type' => 'image',
                    'accept' => '',
                    'icons' => array(
                        array(
                            'name' => 'Photo',
                            'icon' => 'arrow-down'
                        )
                    ),
                    'heading' => 'How to upload ',
                    'list' => array(
                        'Click the <strong>CAMERA</strong> <i class="icon-data-camera common-color"></i> icon below to choose your photos,',
                    )
                ),
                'formPost' => array(
                    'attributes' => array(
                        'method' => 'POST',
                        'route' => array(
                            1 => 'surveysCreateForm',
                            2 => 'surveysUpdateForm'
                        ),
                        'id' => 'formPost',
                        'class' => 'commonContainer'
                    ),
                    'stars' => array(
                        'required' => array(
                            'text' => 'Required',
                            'fieldText' => 'This field is required',
                            'description' => 'Required fields are marked with a red star'
                        ),
                        'optional' => array(
                            'text' => 'Optional',
                            'fieldText' => 'This field is optional but important',
                            'description' => 'Optional fields marked with blue star'
                        )
                    ),
                    'components' => array(
                        'characterReminder' => array(
                            'text' => 'Characters remaining'
                        )
                    ),
                    'submitText' => array(
                        'processing' => 'Processing',
                        1 => 'Save',
                        2 => 'Update',
                        3 => 'Edit'
                    ),
                    'validator' => array(
                        'required' => 'This field is required.',
                        'maxlength' => 'Maximium :length characters allowed',
                        'minlength' => 'Minimum :length characters allowed'
                    ),
                    'hide' => array(
                        1 => array(
                            'htmlNames' => array('')
                        ),
                        2 => array(
                            'htmlNames' => array()
                        )
                    ),
                    'portlets' => array(
                        array(
                            'id' => 'details',
                            'title' => 'Form\'s details',
                            'heading' => 'Please fill in the details of the form.',
                            'help' => 'Please fill in all the mandatory details of this form.',
                            'stared' => 1,
                            'rows' => array(
                                array(
                                    'fields' => array(
                                        array(
                                            'name' => 'Name',
                                            'type' => 'text',
                                            'prepend' => 'user',
                                            'htmlName' => 'name',
                                            'displayed' => 1,
                                            'disabled' => 0,
                                            'placeholder' => 'Type the name eg \'Contact form\'',
                                            'help' => '<strong>Description: </strong>The name of this form.<br/><strong>Do: </strong>Type the name of this form.<br/><strong>Star: </strong> %s <br/><strong>Examples: </strong>\'Contact form\'.',
                                            'validator' => array(
                                                'required' => 1
                                            )
                                        ),
                                    )
                                ),
                                array(
                                    'fields' => array(
                                        array(
                                            'name' => 'Welcome message before starting the survey',
                                            'type' => 'text',
                                            'prepend' => 'user',
                                            'htmlName' => 'welcome_message',
                                            'displayed' => 1,
                                            'disabled' => 0,
                                            'placeholder' => 'Type the welcome message eg \'Welcome to surveychat\'',
                                            'help' => '<strong>Description: </strong>The welcome message of this form.<br/><strong>Do: </strong>Type the welcome message of this form.<br/><strong>Star: </strong> %s <br/><strong>Examples: </strong>\'Welcome to surveychat\'.',
                                            'validator' => array(
                                            )
                                        ),
                                    )
                                ),
                                array(
                                    'fields' => array(
                                        array(
                                            'name' => 'Thank you message after completing the response',
                                            'type' => 'text',
                                            'prepend' => 'user',
                                            'htmlName' => 'thank_you_message',
                                            'displayed' => 1,
                                            'disabled' => 0,
                                            'placeholder' => 'Type the thank you message eg \'Thank you for completing surveychat\'',
                                            'help' => '<strong>Description: </strong>The thank you message of this form.<br/><strong>Do: </strong>Type the thank you message of this form.<br/><strong>Star: </strong> %s <br/><strong>Examples: </strong>\'Thank you for completing surveychat\'.',
                                            'validator' => array(
                                            )
                                        ),
                                    )
                                ),
                                array(
                                    'fields' => array(
                                        array(
                                            'name' => 'Email reponse?',
                                            'type' => 'select',
                                            'prepend' => 'user',
                                            'htmlName' => 'email_response',
                                            'displayed' => 1,
                                            'disabled' => 0,
                                            'placeholder' => 'Select email response',
                                            'help' => '<strong>Description: </strong>Should we email response to.<br/><strong>Do: </strong>Select where we should email response to or not.<br/><strong>Star: </strong> %s ',
                                            'validator' => array(
                                            )
                                        ),
                                        array(
                                            'name' => 'Email response to (comma separated)',
                                            'type' => 'text',
                                            'prepend' => 'user',
                                            'htmlName' => 'email_response_to',
                                            'displayed' => 1,
                                            'disabled' => 0,
                                            'placeholder' => 'Type the email response to eg \'Welcome to surveychat\'',
                                            'help' => '<strong>Description: </strong>The email response to of this form.<br/><strong>Do: </strong>Type the email response to of this form.<br/><strong>Star: </strong> %s <br/><strong>Examples: </strong>\'Welcome to surveychat\'.',
                                            'validator' => array(
                                            )
                                        ),
                                    )
                                ),
                                array(
                                    'fields' => array(
                                        array(
                                            'name' => 'Id',
                                            'type' => 'hidden',
                                            'htmlName' => 'id',
                                        )
                                    )
                                ),
                            )
                        ),
                        array(
                            'id' => 'logo',
                            'title' => 'Images',
                            'heading' => 'Please upload the images of the form.',
                            'help' => 'Please upload the images of the form.',
                            'stared' => 0,
                            'rows' => array(
                                array(
                                    'fields' => array(
                                        array(
                                            'dataSource' => 'mediaView'
                                        )
                                    )
                                )
                            )
                        ),
                    )
                )
            )
        )
    )
);
