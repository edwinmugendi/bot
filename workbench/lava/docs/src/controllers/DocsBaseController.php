<?php

namespace Lava\Docs;

/**
 * S# DocsBaseController() function
 * Docs Base Controller
 * @author Edwin Mugendi
 */
class DocsBaseController extends \BaseController {

    //Package
    public $package = 'docs';

}

//E# DocsBaseController() function