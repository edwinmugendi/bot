<h1 class="homePageSection">Note the following:</h1>
<p>All API's must have parameter \'format\' whose value must be \'json\' so as to return JSON format. Please include it.</p>
<p>In the JSON returned by all API's, I've changed property \'httpStatusCode\' to \'http_status_code\' and \'systemCode\' to \'system_code\'.</p>
<?php echo $view_data['modulesView']; ?>