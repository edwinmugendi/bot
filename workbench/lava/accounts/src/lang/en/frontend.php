<?php

return array(
    /*
      |--------------------------------------------------------------------------
      | Frontend Language Lines
      |--------------------------------------------------------------------------
     */

    'frontendFrontendPage' => array(
        'title' => 'Home'
    ),
    'frontendTermsPage' => array(
        'title' => 'Privacy Policy'
    ),
    'frontendMessengerPage' => array(
        'title' => 'Facebook Messenger'
    ),
    'frontendTelegramPage' => array(
        'title' => 'Telegram'
    ),
);
