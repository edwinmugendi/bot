<header id="header" class="header navbar-fixed-top">  
    <div class="container">    
        <h1 class="logo pull-left"><a class="scrollto" href="#promo"></a></h1>   
        <nav class="main-nav navbar-right" role="navigation">
            <div class="navbar-header text-center">
                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-collapse">
                    <span class="toggle-title">Menu</span>
                    <span class="icon-bar-wrapper">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </span><!--//icon-bar-wrapper-->
                </button><!--//nav-toggle-->
            </div><!--//navbar-header-->
            <div id="navbar-collapse" class="navbar-collapse collapse text-center">
                <ul class="nav navbar-nav center-block">
                    <li class="nav-item"><a class="scrollto" href="#about">About</a></li>
                    <li class="nav-item"><a class="scrollto" href="#video">Video</a></li>
                    <li class="nav-item"><a class="scrollto" href="#contact">Contact</a></li>  
                    <li class="nav-item"><a href="<?php echo \URL::route('userRegistrationLogin') ?>">Login</a></li>
                    <li class="nav-item"><a href="<?php echo \URL::route('userRegistrationLogin') ?>#toregister">Sign up</a></li>
                </ul><!--//nav-->
            </div><!--//navabr-collapse-->
        </nav><!--//main-nav-->
    </div><!--//container-->
</header><!--//header-->

<?php echo $view_data['sub_page']; ?>
<!-- ******FOOTER****** --> 
<footer class="footer">
    <div class="container">
        <div class="footer-content text-center">
            <div class="social-container">
                <ul class="list-inline social-list">
                    <li><a target="_blank" href="https://twitter.com/surveychat"><i class="fa fa-twitter"></i></a></li>
                </ul><!--//social-list-->
            </div><!--//social-container-->
            <div class="copyright">&copy; <?php echo date('Y'); ?>&nbsp;<?php echo \Config::get('product.name'); ?> | All rights reserved </div>  
        </div><!--//footer-content--> 
    </div><!--//container-->
</footer><!--//footer-->
