<section id="promo" class="section promo-section bg-gradient">
    <div class="container">
        <h2 class="headline text-center">Create Surveys <br class="visible-sm-block"> & Get Answers via Facebook Messenger or Telegram</h2>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-11 col-md-offset-1" style="margin-bottom: 40px;">
                        <a class="btn btn-lg btn-primary" href="https://www.facebook.com/surveychatbot" target="_blank">Go to @surveychatbot on Messenger</a>
                    </div><!--//product-holder-->
                </div><!--//overview-wrapper-->
                <div class="overview-wrapper row">
                    <div class="product-holder col-md-12">
                        <img class="img-responsive product-image" src="<?php echo asset('img/homePage/messenger.png'); ?>" alt="">
                        <div class="control text-center">
                            <a id="video-play-triggger" class="video-play-trigger" data-toggle="modal" data-target="#modal-facebook"><i class="fa fa-play"></i></a>                   
                        </div>
                    </div><!--//product-holder-->
                </div><!--//overview-wrapper-->
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-11 col-md-offset-1" style="margin-bottom: 40px;">
                        <a class="btn btn-lg btn-primary" href="http://telegram.me/surveychatbot" target="_blank">Go to @surveychatbot on Telegram</a>
                    </div><!--//product-holder-->
                </div><!--//overview-wrapper-->
                <div class="overview-wrapper row">
                    <div class="product-holder col-md-12">
                        <img class="img-responsive product-image" src="<?php echo asset('img/homePage/telegram.png'); ?>" alt="">
                        <div class="control text-center">
                            <a id="video-play-triggger" class="video-play-trigger" data-toggle="modal" data-target="#modal-telegram"><i class="fa fa-play"></i></a>                   
                        </div>
                    </div><!--//product-holder-->
                </div><!--//overview-wrapper-->
            </div>
        </div><!--//overview-wrapper-->
        <!--//overview-wrapper-->
    </div><!--//container-->
</section><!--//promo-section-->
<section id="about" class="about-section">
    <div class="container">
        <h2 class="section-title text-center">What is <?php echo \Config::get('product.name'); ?>?</h2>
        <div class="item">
            <div class="row">
                <div class="desc-holder col-xs-12 col-sm-5">
                    <h3 class="item-title">Create surveys</h3>
                    <div class="item-desc">
                        <p>Easily design and publish surveys online</p>
                        <p>
                            Get contacts, fill applications and serve your customers better by asking simple questions via Telegram
                        </p>
                        <h4 class="item-title">You can collect the following:</h4>
                        <ul class="list-unstyled list-custom">
                            <li><i class="fa fa-check"></i>Text</li>
                            <li><i class="fa fa-check"></i>Integer</li>
                            <li><i class="fa fa-check"></i>Decimal</li>
                            <li><i class="fa fa-check"></i>Radio (Single Select)</li>
                            <li><i class="fa fa-check"></i>GPS co-ordinates</li>
                            <li><i class="fa fa-check"></i>Photos</li>
                             <li><i class="fa fa-check"></i>Videos</li>
                            <li><i class="fa fa-check"></i>Integrate to NPL such as API.AI, WIT.AI</li>
                            <li><i class="fa fa-check"></i>Get data from API</li>
                        </ul>
                    </div>
                </div><!--//desc-holder-->
                <div class="figure-holder col-xs-12 col-sm-7">
                    <div class="figure-holder-inner figure-right">
                        <img class="img-responsive" src="<?php echo asset('img/homePage/questions.png'); ?>" alt="<?php echo \Config::get('product.name'); ?> list of questions">
                    </div><!--//figure-holder-inner-->
                </div><!--//figure-holder-->
            </div><!--//row-->
        </div><!--//item-->
        <div class="item">
            <div class="row">

                <div class="figure-holder figure-holder-left col-xs-3 col-sm-4">
                    <div class="figure-holder-inner figure-left">
                        <img class="img-responsive" src="<?php echo asset('img/homePage/messenger.png'); ?>" alt="<?php echo \Config::get('product.name'); ?> screenshot">
                    </div><!--//figure-holder-inner-->
                </div><!--//figure-holder-->
                <div class="desc-holder col-xs-3 col-sm-4">
                    <h3 class="item-title">Get answers via Facebook Messenger or Telegram</h3>
                    <div class="item-desc">
                        <p>Quickly and easily get responses via  Facebook Messenger or Telegram</p>
                        <ul class="list-unstyled list-custom">
                            <li><i class="fa fa-check"></i>It's easy</li>
                            <li><i class="fa fa-check"></i>It's fast</li>
                            <li><i class="fa fa-check"></i>It's reliable</li>
                            <li><i class="fa fa-check"></i>It's realtime</li>
                            <li><i class="fa fa-check"></i>Chatting is what millennials are used to</li>
                        </ul>
                    </div>
                </div><!--//desc-holder-->
                <div class="figure-holder figure-holder-left col-xs-3 col-sm-4">
                    <div class="figure-holder-inner figure-left">
                        <img class="img-responsive" src="<?php echo asset('img/homePage/telegram.png'); ?>" alt="<?php echo \Config::get('product.name'); ?> screenshot">
                    </div><!--//figure-holder-inner-->
                </div><!--//figure-holder-->
            </div><!--//row-->
        </div><!--//item-->
        <div class="item">
            <div class="row">
                <div class="desc-holder col-xs-12 col-sm-5">
                    <h3 class="item-title">Get answers in real time</h3>
                    <div class="item-desc">
                        <p>Get information in real time</p>
                        <ul class="list-unstyled list-custom">
                            <li><i class="fa fa-check"></i>Export data to CSV, PDF or Excel</li>
                            <li><i class="fa fa-check"></i>Geo-spatially analyse your data on Google Maps</li>
                            <li><i class="fa fa-check"></i>Get notified via email</li>
                            <li><i class="fa fa-check"></i>ISN (Instant Survey Notification) - Get data pushed to your API</li>
                        </ul>
                    </div>
                </div><!--//desc-holder-->
                <div class="figure-holder col-xs-12 col-sm-7">
                    <div class="figure-holder-inner figure-right">
                        <img class="img-responsive" src="<?php echo asset('img/homePage/data.png'); ?>" alt="<?php echo \Config::get('product.name'); ?> collected data">
                    </div><!--//figure-holder-inner-->
                </div><!--//figure-holder-->
            </div><!--//row-->
        </div><!--//item-->
    </div><!--//container-->
</section><!--//section-->
<section id="video" class="updates-section">
    <div class="container text-center">
        <div class="row">
            <div class="col-md-6">
                <h2 class="section-title">Facebook Messenger Demo</h2>
                <div class="item">
                    <div class="item-content">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/78fUiKgYVDw?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div><!--//item-content-->
                </div><!--//item-->
            </div>
            <div class="col-md-6">
                <h2 class="section-title">Telegram Demo</h2>
                <div class="item">
                    <div class="item-content">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/3cTBGAu-3nk?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div><!--//item-content-->
                </div><!--//item-->
            </div>
        </div>

    </div><!--//container-->
</section><!--//updates-section-->

<section id="contact" class="contact-section section">
    <h2 class="section-title text-center">Want to contact us?</h2>
    <div class="contact-form-container">    
        <p class="text-center"><i class="fa fa-envelope"></i>&nbsp;<a href="mailto:chat@sapamatech.com">chat@sapamatech.com</a></p>
    </div><!--//contact-form-container-->

</section><!--//contact-container-->

<section id="faq" class="section faq-section">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <h2 class="text-center">Sign up today to use <?php echo \Config::get('product.name'); ?></h2>
                <h3 class="text-center">Sign up to create surveys and collect data via Facebook Messenger and Telegram</h3> 
                <a class="button btn btn-lg btn-primary center-block" style="width: 200px;" href="<?php echo \URL::route('userRegistration') ?>#toregister">Sign up</a>
                <h3 class="text-center">Already designed a form?</h3> 
                <div class="row">
                    <div class="col-md-6">
                        <a class="btn btn-lg btn-primary center-block" target="_blank" href="https://www.facebook.com/surveychatbot" style="width:auto;">Go to @surveychatbot on Messenger</a>

                    </div>
                    <div class="col-md-6">
                        <a class="btn btn-lg btn-primary center-block" target="_blank" href="http://telegram.me/surveychatbot" style="width:auto;">Go to @surveychatbot on Telegram</a>

                    </div>
                </div>
            </div>
        </div><!--//row-->
    </div><!--//container-->
</section><!--//section-->

<!-- Video Modal -->
<div class="modal modal-video" id="modal-telegram" tabindex="-1" role="dialog" aria-labelledby="videoModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 id="videoModalLabel" class="modal-title sr-only">Product Video</h4>
            </div>
            <div class="modal-body">
                <div class="video-container">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/3cTBGAu-3nk?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div><!--//video-container-->
            </div><!--//modal-body-->
        </div><!--//modal-content-->
    </div><!--//modal-dialog-->
</div><!--//modal-->

<!-- Video Modal -->
<div class="modal modal-video" id="modal-facebook" tabindex="-1" role="dialog" aria-labelledby="videoModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 id="videoModalLabel" class="modal-title sr-only">Product Video</h4>
            </div>
            <div class="modal-body">
                <div class="video-container">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/78fUiKgYVDw?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div><!--//video-container-->
            </div><!--//modal-body-->
        </div><!--//modal-content-->
    </div><!--//modal-dialog-->
</div><!--//modal-->
