
<section id="contact" class="contact-section section">
    <h2 class="section-title text-center">Privacy Policy?</h2>
    <div class="contact-form-container">    
        <p>Last updated: April 26, 2017</p>

        <p>We operates the https://SurveyChat.co website (the "Service"), @surveychat bot on Facebook Messenger and @surveychatbot on Telegram.</p>

        <p>This page informs you of our policies regarding the collection, use and disclosure of Personal Information when you use our Service.</p>

        <p>We will not use or share your information with anyone except as described in this Privacy Policy.</p>
        <p>We use your Personal Information for providing and improving the Service. By using the Service, you agree to the collection and use of information in accordance with this policy. Unless otherwise defined in this Privacy Policy, terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, accessible at https://SurveyChat.co</p>

        <h2>Information Collection And Use</h2>

        <p>While using our Service, you design forms with a set of questions and you are able to collect data via Facebook Messenger and Telegram Bot. Data collected is saved in our database and you can login, download it in form of excel, csv, or pdf. Also, you can also delete the data.</p>
        <h2>Log Data</h2>
        <p>We collect information that that are sent from the bots. This Log Data data include the message identifier and recipient id from the bots. The actual message is masked.</p>
        <h2>Service Providers</h2>
        <p>We may employ third party companies and individuals to facilitate our Service, to provide the Service on our behalf, to perform Service-related services or to assist us in analyzing how our Service is used.</p>

        <p>These third parties don't have access to your personal information.</p>

        <h2>Security</h2>

        <p>The security of your Personal Information is important to us, but remember that no method of transmission over the Internet, or method of electronic storage is 100% secure. While we strive to use commercially acceptable means to protect your Personal Information, we cannot guarantee its absolute security.</p>

        <h2>Links To Other Sites</h2>

        <p>Our Service may contain links to other sites that are not operated by us. If you click on a third party link, you will be directed to that third party's site. We strongly advise you to review the Privacy Policy of every site you visit.</p>

        <p>We have no control over, and assume no responsibility for the content, privacy policies or practices of any third party sites or services.</p>

        <h2>Changes To This Privacy Policy</h2>

        <p>We may update our Privacy Policy from time to time. We will notify you of any changes by posting the new Privacy Policy on this page.</p>

        <p>You are advised to review this Privacy Policy periodically for any changes. Changes to this Privacy Policy are effective when they are posted on this page.</p>

        <h2>Contact Us</h2>

        <p>If you have any questions about this Privacy Policy, please contact us on <a href="mailto:chat@sapamatech.com">chat@sapamatech.com.</p>
    </div><!--//contact-form-container-->
</section><!--//contact-container-->
