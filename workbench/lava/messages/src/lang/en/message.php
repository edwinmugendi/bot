<?php

return array(
    /*
      |--------------------------------------------------------------------------
      | Message Language Lines
      |--------------------------------------------------------------------------
     */
    'communication' => array(
        'form' => array(
            'email' => ':productName :formname :status',
            'sms' => 'Kindly check your email to activate your :productName account',
            'push' => ''
        ),
        'activateAccount' => array(
            'email' => 'Activate your :productName account',
            'sms' => 'Kindly check your email to activate your :productName account',
            'push' => ''
        ),
        'notice' => array(
            'email' => ':productName :urgent',
            'sms' => ':productName urgent',
            'push' => ''
        ),
        'verify' => array(
            'email' => 'Request to change your :productName account password.',
            'sms' => 'Welcome to :productName. Your code is :code. Enter it in the app to verify your account.',
            'push' => ''
        ),
        'pin' => array(
            'email' => ':',
            'sms' => ':code is your PIN. Enter this pin in app to continue. Welcome back to :productName.',
            'push' => ''
        ),
        'forgotPassword' => array(
            'email' => 'Request to change your :productName account password.',
            'sms' => 'Dear :name, your :productName password reset code is :resetCode.',
            'push' => ''
        ),
        'welcome' => array(
            'email' => 'Welcome to :productName',
            'sms' => 'Welcome to :productName',
            'push' => ''
        ),
        'registration' => array(
            'email' => 'Welcome to :productName',
            'sms' => 'Welcome to :productName',
            'push' => ''
        ),
        'contact' => array(
            'email' => ':productName action required',
            'sms' => 'Welcome to :productName',
            'push' => ''
        )
    )
);

