<?php if ($view_data['action'] == 'approved'): ?>
    <p>Dear <?php echo $view_data['name']; ?>,</p>
    <p class="lead" style="color: #222222; font-family: 'Helvetica Neue', 'Arial', sans-serif; font-weight: normal; text-align: left; line-height: 25px; font-size: 16px; margin: 0 0 25px; padding: 0;" align="left">
        Congratulations! Your survey <strong><?php echo $view_data['formname'] ?></strong> has been approved.
    </p>
    <p class="lead" style="color: #222222; font-family: 'Helvetica Neue', 'Arial', sans-serif; font-weight: normal; text-align: left; line-height: 25px; font-size: 16px; margin: 0 0 25px; padding: 0;" align="left">
        How to conduct the survey:<br>
        1. Install Facebook Messenger Telegram app on your phone<br>
        2. For Telegram, go to or search <strong>@surveychatbot</strong><br>
        2. For Facebook Messenger, go to <strong><a href="http://m.me/surveychatbot">http://m.me/surveychatbot</strong><br>
        3. Type <i><strong>/fill <?php echo $view_data['formname'] ?></strong></i> to fill in the <strong><?php echo $view_data['formname'] ?></strong> survey<br>
        4. Also, watch these youtube videos:<br>
        a). <a href="https://youtu.be/78fUiKgYVDw">Facebook Messenger</a>, or click <a href="https://youtu.be/78fUiKgYVDw">https://youtu.be/78fUiKgYVDw</a><br>
        b). <a href="https://youtu.be/3cTBGAu-3nk">Telegram</a>, or click <a href="https://youtu.be/3cTBGAu-3nk">https://youtu.be/3cTBGAu-3nk</a><br>
    </p>
<?php endif; ?>
