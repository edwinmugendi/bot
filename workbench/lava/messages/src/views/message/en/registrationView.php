<h3 style="color: #222222; font-family: 'Helvetica Neue', 'Arial', sans-serif; font-weight: normal; text-align: left; line-height: 1.3; word-break: normal; font-size: 30px; margin: 0; padding: 15px 0;" align="left">
    Dear <?php echo $view_data['name']; ?>,
</h3>
<p class="lead" style="color: #222222; font-family: 'Helvetica Neue', 'Arial', sans-serif; font-weight: normal; text-align: left; line-height: 25px; font-size: 16px; margin: 0 0 25px; padding: 0;" align="left">
    Thanks for your interest in <?php echo $view_data['productName']; ?>.
</p>
<p class="lead" style="color: #222222; font-family: 'Helvetica Neue', 'Arial', sans-serif; font-weight: normal; text-align: left; line-height: 25px; font-size: 16px; margin: 0 0 25px; padding: 0;" align="left">
    <?php echo $view_data['productName']; ?> enables you to create surveys online and get answers in real time via Telegram. Facebook Messenger is coming soon.
</p>
<p class="lead" style="color: #222222; font-family: 'Helvetica Neue', 'Arial', sans-serif; font-weight: normal; text-align: left; line-height: 25px; font-size: 16px; margin: 0 0 25px; padding: 0;" align="left">
    <?php echo $view_data['productName']; ?> currently supports collecting text, integer, decimal, radio (Single Select), gps and photos.
</p>
<p class="lead" style="color: #222222; font-family: 'Helvetica Neue', 'Arial', sans-serif; font-weight: normal; text-align: left; line-height: 25px; font-size: 16px; margin: 0 0 25px; padding: 0;" align="left">
    How to test the bot:<br>
    1. Install Telegram Messenger <a href="https://play.google.com/store/apps/details?id=org.telegram.messenger">Android</a> or <a href="https://itunes.apple.com/us/app/telegram-messenger/id686449807?mt=8">IOS</a> app if not installed<br>
    2. Go to <strong>@surveychatbot</strong><br>
    3. Type <i><strong>/fill contact</strong></i> to test the contact form<br>
    4. Or watch this <a href="https://youtu.be/3cTBGAu-3nk">youtube video</a> on how the bot works
</p>
<p class="lead" style="color: #222222; font-family: 'Helvetica Neue', 'Arial', sans-serif; font-weight: normal; text-align: left; line-height: 25px; font-size: 16px; margin: 0 0 25px; padding: 0;" align="left">
    How to test the backend:<br>
    1. Go to: <a href="http://surveychat.co/login">http://surveychat.co/login</a><br>
    2. Login with email: <strong>edwinmugendi@gmail.com</strong> and password:<strong>123456</strong><br>
</p>

<p class="lead" style="color: #222222; font-family: 'Helvetica Neue', 'Arial', sans-serif; font-weight: normal; text-align: left; line-height: 25px; font-size: 16px; margin: 0 0 25px; padding: 0;" align="left">
    We'll be contacting you soonest possible to complete activation of your account and give you access to <?php echo $view_data['productName']; ?>.
</p>
