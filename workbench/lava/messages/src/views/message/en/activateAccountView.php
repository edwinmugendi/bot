<h1>Welcome to <?php echo $view_data['productName']; ?></h1>
<p>Dear <?php echo $view_data['name']; ?>,</p>
<p class="lead" style="color: #222222; font-family: 'Helvetica Neue', 'Arial', sans-serif; font-weight: normal; text-align: left; line-height: 25px; font-size: 16px; margin: 0 0 25px; padding: 0;" align="left">
    Congratulations! You account has been activated. 
</p>
<p class="lead" style="color: #222222; font-family: 'Helvetica Neue', 'Arial', sans-serif; font-weight: normal; text-align: left; line-height: 25px; font-size: 16px; margin: 0 0 25px; padding: 0;" align="left">
    <?php echo $view_data['productName']; ?> enables you to create surveys online and get answers in real time via Telegram. Facebook Messenger is coming soon.
</p>

<p class="lead" style="color: #222222; font-family: 'Helvetica Neue', 'Arial', sans-serif; font-weight: normal; text-align: left; line-height: 25px; font-size: 16px; margin: 0 0 25px; padding: 0;" align="left">
    To set your password and activate your account, please click on the link below (or copy and paste the link (URL) onto your browser):
</p>

<p><?php echo $view_data['url']; ?></p>

